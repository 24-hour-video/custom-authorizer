'use strict';
const https = require('https');
const jose = require('node-jose')
const promisify = require('util').promisify

const region = process.env.REGION || 'eu-central-1'
const userPoolID = process.env.USER_POOL_ID || 'eu-central-1_pM0dKajsf'
const appClientID = process.env.APP_CLIENT_ID || '4237hk384ilkbo5ftvkn7ijkk8'
const keysURL = `https://cognito-idp.${region}.amazonaws.com/${userPoolID}/.well-known/jwks.json`;

const generatePolicy = (principalId, effect, resource) => {
  if (effect && resource) {
    const policyDocument = {
      Version: '2012-10-17',
      Statement: [
        {
          Action: 'execute-api:Invoke',
          Effect: effect,
          Resource: resource,
        }
      ]
    }

    return { principalId, policyDocument }
  }
}

exports.handler = async (event, context) => {
  console.log(event)
  if (!event.authorizationToken) {
    throw new Error('No authorizationToken')
  }

  const authorizationToken = event.authorizationToken
  const sections = authorizationToken.split('.')
  let header;
  try {
    header = JSON.parse(jose.util.base64url.decode(sections[0]));
  } catch(error) {
    throw new Error('Unauthorized')
  }
  const kid = header.kid
  const body = await httpsGet(keysURL);
  const keys = JSON.parse(body)['keys']
  const key = keys.find(key => key.kid === kid)
  if (!key) {
    throw new Error('No key found in keys')
  }
  return jose.JWK.asKey(key)
    .then(result => {
      console.log("jose verification",jose.JWS.createVerify(result))
      return jose.JWS.createVerify(result)
        .verify(authorizationToken)
    })
    .then(result => {
      const claims = JSON.parse(result.payload);
      const now =  Math.floor(new Date() / 1000)
      if (now > claims.exp) {
        throw new Error('authorizationToken is expired')
      }

      if (claims.aud != appClientID) {
        throw new Error('authorizationToken was not issued for this audience')
      }
      return generatePolicy('user', 'allow', event.methodArn)
    })
    .catch(err => {
      throw new Error('Unauthorized')
    })
};

const httpsGet = (URL) => {
  return new Promise((resolve, reject) => {
    const req = https.get(URL, (response) => {
      let data = '';
      response.on('data', (chunk) => {
        data += chunk
      })
      response.on('end', () => {
        resolve(data)
      })
    })

    req.on('error', (e) => {
      reject(e)
    })

  })
}